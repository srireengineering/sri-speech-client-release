# SRISpeechClient #

The SRI Speech Client is a javascript library  that provides an interface to Cengage SRI services (using the latest Dynaspeak engine). It provides the capability to record, stream and receive scores of audio files analyzed by SRI engine. 

This is an initial specification and is expected to change / evolve in future as we develop the library.

